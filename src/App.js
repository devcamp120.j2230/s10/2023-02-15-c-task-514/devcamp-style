import image from './assets/images/48.jpg'

function App() {
  return (
    <div>
      <img src = {image} width="128px"/>
      <p>This is one of the best developer blogs on the planet! I read it daily to improve my skills</p>
      <p><b>Tammy Stevens</b> Front End Developer</p>
    </div>
  );
}

export default App;
